# SVR_Permeability

## Name
Permeability predicition using support vector regression for FiberForm and PICA-like porous materials.

## Description
A simple python code to predict permeability of porous materials for a wide range of porosity, temperature and pressure values.


## Usage
Run the respective python files with the following command "python3 svr_Fiber_predict species porosity temperature pressure" to find the permeability of FiberForm and "python3 svr_picalike_predict species degree_of_char temperature pressure" to find the  variation of permeability with degradtion of the PICA-like porous material. Replace numerical values in place of the input variables specified in the command to compute permeability.

Make sure to download the respective txt files associated with each code which contains the support vectors. The python code is designed to read the support vectors from their respective txt files.

The code also gives feedback (error messages) if the command line has insufficient number of inputs or if the numerical values do not fall in a particular range. 

## Support
Please contact the PI of the lab Dr. Savio J. Poovathingal (Email id: saviopoovathingal@uky.edu) for any queries regarding the usage of the code
