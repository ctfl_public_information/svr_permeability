#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 29 05:04:44 2022

@author: vijaybmohan
"""

def error(str=None):
  if str: print("ERROR:",str)
  else:
      print('Please use the right syntax to run the file and make sure that the support_vectors_Fiberform.txt file is present in the same folder\n')
      print("Syntax: SVR_Fiber_predict.py Species Porosity Temperature Pressure")
  sys.exit()

def err(str=None):
    if str:
        print("ERROR:",str)
    else:
        print('Please enter the value of porosity between 0.8 and 0.9\n')
    sys.exit()
        
def er(str=None):
    if str:
        print("ERROR:",str)
    else:
        print('Please enter one of the species among O2, N2, Ar, CO and CO2 \n')
    sys.exit()

import sys
import numpy as np
import math

if len(sys.argv) != 5: error()

if float(sys.argv[2]) < 0.8 or float(sys.argv[2]) > 0.9:
    err()
    
if sys.argv[1] != 'CO' and sys.argv[1] != 'CO2' and sys.argv[1] != 'Ar' and sys.argv[1] != 'N2' and sys.argv[1] != 'O2':
    er()

#Read Data
support_vectors=np.zeros((9,4))
coef=np.zeros((9,1))
s_out=tuple()
f_out=open('Support_vectors_Fiberform.txt','r')
for num, line in enumerate(f_out, 1):
    s_out=line.split()
    if num>1 and len(s_out)!=0:
        s=line.split()
        n=len(s_out)
        support_vectors[num-2,0] = float(s[0])
        support_vectors[num-2,1] = float(s[1])
        support_vectors[num-2,2] = float(s[2])
        support_vectors[num-2,3] = float(s[3])
        coef[num-2,0] = float(s[4])
        intercept = float(s[5])
        
#Define Species dictionary
specis_list=dict(CO='9.93',N2='9.67',Ar='9.63',O2='9.541',CO2='7.67')

#testing
t=[sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4]]
t_m=np.zeros((4,))
for j in range(4):
    if j == 0:
        t_m[j]=float(specis_list[t[j]])
    else:
        t_m[j]=math.log(float(t[j]))

#Support vector check
terms=0
svr_predict=0
for i in range(len(support_vectors)):
    distance=0
    for j in range(4):
        distance=distance+(support_vectors[i,j]-t_m[j])**2
    terms=terms+(coef[i,0]*(np.exp(-0.01*distance)))
svr_predict=terms+intercept
svr_predict=np.exp(svr_predict)

print('%s m^2' %svr_predict)